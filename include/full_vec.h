#ifndef FULL_VEC_H_
#define FULL_VEC_H_

#include <mpi.h>
#include <cstring>
#include <iostream>

#include "global.h"
#include "range.h"

template<typename T>
class FullVec {
public:
	FullVec(int n);
	~FullVec();

	bool operator==(const FullVec<T> &it) const;
	FullVec<T>& operator=(const FullVec<T> &it);

	T& operator[](size_t i);
	const T& operator[](size_t i) const;

	void fill(const T &v);
	void setToIndex();

	T* getPtr();
	const T* getPtr() const;
	int getN() const;

	void debugPrint(const char *s = "~:");

private:
	int n;
	int rank, size;
	int nthreads;
	int length;  // number of elements owned by the current process
	int *pRange; // process (whole vector)
	int *tRange; // thread  (whole vector)
	int *eRange; // thread  (each process)
	int *eq_;
	T *ptr_;
};

template<typename T>
FullVec<T>::FullVec(int n):n(n)
{
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	nthreads = get_num_threads();
	pRange = new int[size + 1];
	tRange = new int[nthreads + 1];
	eRange = new int[nthreads + 1];
	eq_ = new int[nthreads];

	getRange(n, size, pRange);
	getRange(n, nthreads, tRange);
	length = pRange[rank + 1] - pRange[rank];
	getRange(length, nthreads, eRange);

	ptr_ = new T[n];
}

template<typename T>
FullVec<T>::~FullVec()
{
	delete[] pRange;
	delete[] tRange;
	delete[] eRange;
	delete[] eq_;
	delete[] ptr_;
}

template<typename T>
bool FullVec<T>::operator==(const FullVec<T> &it) const
{
	int offset = pRange[rank];
	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < nthreads; t++)
		eq_[t] = !memcmp(ptr_ + offset + eRange[t], it.ptr_ + offset + eRange[t],
				sizeof(T) * (eRange[t + 1] - eRange[t]));
	int eq = 1;
	for (int t = 0; t < nthreads && eq; t++)
		eq = eq_[t];
	return allReduce(eq, MPI_LAND);
}

template<typename T>
FullVec<T>& FullVec<T>::operator=(const FullVec<T> &it)
{
	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < nthreads; t++)
		std::copy(it.ptr_ + tRange[t], it.ptr_ + tRange[t + 1],
				ptr_ + tRange[t]);
	return *this;
}


template<typename T>
T& FullVec<T>::operator[](size_t i) {
	return ptr_[i];
}

template<typename T>
const T& FullVec<T>::operator[](size_t i) const {
	return ptr_[i];
}

template<typename T>
void FullVec<T>::fill(const T &v) {
	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < nthreads; t++)
		std::fill(ptr_ + tRange[t], ptr_ + tRange[t + 1], v);
}

template<typename T>
void FullVec<T>::setToIndex() {
	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < nthreads; t++)
		for (int i = tRange[t]; i < tRange[t + 1]; i++)
			ptr_[i] = i;
}

template<typename T>
T* FullVec<T>::getPtr() {
	return ptr_;
}

template<typename T>
const T* FullVec<T>::getPtr() const {
	return ptr_;
}

template<typename T>
int FullVec<T>::getN() const {
	return n;
}

template<typename T>
void FullVec<T>::debugPrint(const char *s) {
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0) {
		std::cout << s;
		for (int i = 0; i < n; i++)
			std::cout << " " << ptr_[i];
		std::cout << std::endl;
	}
	MPI_Barrier(MPI_COMM_WORLD);
}

#endif /* FULL_VEC_H_ */
