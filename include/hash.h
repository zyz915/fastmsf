#ifndef HASH_H_
#define HASH_H_

const int P = 1024;

#define HASH(x) (((x << 4) + x) & 1023)
#define NEXT(x) ((x + 23) & 1023)

class HashTable {
public:

	void init() {
		memset(ht_key, -1, sizeof(int) * P);
		memset(ht_val,  0, sizeof(int) * P);
	}

	int hash(int key) {
		int h = HASH(key);
        while (ht_key[h] != -1 && ht_key[h] != x)
            h = NEXT(h);
        return h;
	}

	void sample(int *I, int n, int samples = 864) {
	    for (int i = 0; i < samples; i++) {
	        int x = I[rand() % n];
	        int h = this->hash(x);
	        ht_key[h] = x;
	        ht_val[h] += 1;
	    }
	}

	int ht_key[P];
	int ht_val[P];
};

#endif /* HASH_H_ */
