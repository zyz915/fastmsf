#ifndef API_H_
#define API_H_

#include <mpi.h>
#include <vector>

#include "dist_vec.h"
#include "full_vec.h"
#include "range.h"

using std::vector;

static double t_sync_sr = 0;
static double t_sync_ag = 0;
static double t_sync_ib = 0;

void printSyncTime() {
	// printf("Sendrecv %f\n", t_sync_sr);
	printf("Allgather %f\n", t_sync_ag);
}

template<typename T>
void sync_bcast (const DistVec<T> &col, FullVec<T> &row)
{
double t1 = MPI_Wtime();
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int n = row.getN();
	int *rdispls = new int[size + 1];
	getRange(n, size, rdispls);
	int sendcnt = rdispls[rank + 1] - rdispls[rank];
	std::copy(col.getPtr(), col.getPtr() + sendcnt,
			row.getPtr() + rdispls[rank]);

	MPI_Request *requests = new MPI_Request[size];
	MPI_Status  *statuses = new MPI_Status[size];
	for (int i = 0; i < size; i++) {
		T *ptr = row.getPtr() + rdispls[i];
		int count = rdispls[i + 1] - rdispls[i];

		MPI_Ibcast(ptr, count, MPIType<T>(), i,
				MPI_COMM_WORLD, &requests[i]);
	}
	MPI_Waitall(size, requests, statuses);

	delete[] requests;
	delete[] statuses;
	delete[] rdispls;
double t2 = MPI_Wtime();
t_sync_ib += t2 - t1;
}

template<typename T>
void sync_sendrecv (const DistVec<T> &col, FullVec<T> &row)
{
double t1 = MPI_Wtime();
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int n = row.getN();
	int *rdispls = new int[size + 1];
	getRange(n, size, rdispls);
	int sendcnt = rdispls[rank + 1] - rdispls[rank];

	const T *cPtr = col.getPtr();

	MPI_Status status;
	for (int i = 1; i < size; i++) {
		int to = (rank + i) % size;
		int fr = (rank + size - i) % size;
		int recvcnt = rdispls[fr + 1] - rdispls[fr];
		T *rPtr = row.getPtr() + rdispls[fr];

		MPI_Sendrecv(
			cPtr, sendcnt, MPIType<T>(), to, 0,
			rPtr, recvcnt, MPIType<T>(), fr, 0,
			MPI_COMM_WORLD, &status);
	}
	T *rPtr = row.getPtr() + rdispls[rank];
	std::copy(cPtr, cPtr + sendcnt, rPtr);

	delete[] rdispls;
double t2 = MPI_Wtime();
t_sync_sr += t2 - t1;
}

template<typename T>
void sync_allgather (const DistVec<T> &col, FullVec<T> &row)
{
double t1 = MPI_Wtime();
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int n = row.getN();
	int *recvcnt = new int[size];
	int *rdispls = new int[size + 1];
	getRange(n, size, rdispls);
	for (int i = 0; i < size; i++)
		recvcnt[i] = rdispls[i + 1] - rdispls[i];
	int sendcnt = recvcnt[rank];

	const T *cPtr = col.getPtr();
	T *rPtr = row.getPtr();

	MPI_Allgatherv(
			cPtr, sendcnt, MPIType<T>(),
			rPtr, recvcnt, rdispls, MPIType<T>(),
			MPI_COMM_WORLD);

	delete[] recvcnt;
	delete[] rdispls;
double t2 = MPI_Wtime();
t_sync_ag += t2 - t1;
}

template<typename T>
void sync_sendrecv_sparse (const DistVec<T> &col, FullVec<T> &row)
{
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int n = row.getN();
	int st  = getStart(n, size, rank);
	int len = getLength(n, size, rank);

	int nthreads = get_num_threads();
	int *count = new int[nthreads];
	int *displs = new int[nthreads + 1];
	std::fill(count, count + nthreads, 0);

	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
	{
		int xst = getStart(len, nthreads, t);
		int xed = getEnd(len, nthreads, t);
		for (int i = xst; i < xed; i++)
			if (col[i] < row[i + st])
				count[t] += 1;
	}

	displs[0] = 0;
	for (int i = 0; i < nthreads; i++)
		displs[i + 1] = displs[i] + count[i];
	std::fill(count, count + nthreads, 0);
	int sendcnt = displs[nthreads];

	int *sind = new int[sendcnt];
	T   *sval = new   T[sendcnt];

	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
	{
		int xst = getStart(len, nthreads, t);
		int xed = getEnd(len, nthreads, t);
		for (int i = xst; i < xed; i++)
			if (col[i] < row[i + st]) {
				sind[displs[t] + count[t]] = i + st;
				sval[displs[t] + count[t]] = col[i];
				count[t] += 1;
			}
	}

	int *recvcnt = new int[size];
	int *rdispls = new int[size + 1];
	MPI_Allgather(&sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD);
	rdispls[0] = 0;
	for (int i = 0; i < size; i++)
		rdispls[i + 1] = rdispls[i] + recvcnt[i];
	// allgatherv
	int totrecv = rdispls[size];
	int *rind = new int[totrecv];
	T   *rval = new   T[totrecv];
	MPI_Allgatherv(
			sind, sendcnt, MPI_INT,
			rind, recvcnt, rdispls, MPI_INT,
			MPI_COMM_WORLD);
	MPI_Allgatherv(
			sval, sendcnt, MPIType<T>(),
			rval, recvcnt, rdispls, MPIType<T>(),
			MPI_COMM_WORLD);
	delete[] sind;
	delete[] sval;
	delete[] recvcnt;
	delete[] rdispls;

	T *res = row.getPtr();
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < totrecv; i++)
		res[rind[i]] = rval[i];

	delete[] rind;
	delete[] rval;
}

struct AssignParam {
	int rank, size;
	int threshold;  // used for MPI_Allreduce()
	int totrecv;
	double t_prepare;

	vector<int> sind, rind;

	AssignParam() {
		threshold = 0;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		MPI_Comm_size(MPI_COMM_WORLD, &size);
	}
};

template<typename I = int, typename M = char>
void get_assign_param (const DistVec<I> &index, const DistVec<M> &mask, AssignParam &param)
{
param.t_prepare = MPI_Wtime();
	int n = index.getN();
	int size = param.size, rank = param.rank;
	int st = getStart(n, size, rank);
	int len = getLength(n, size, rank);
	// a parameter that needs tuning
	int nthreads = get_num_threads();
	int chunksize = 4096;
	int nChunks = n / chunksize / size;
	nChunks = std::min(nChunks, 16 * nthreads);
	nChunks = std::max(nChunks, nthreads);

	int *count = new int[nChunks * nthreads];
	int *rsum = new int[nChunks];
	memset(count, 0, sizeof(int) * nChunks * nthreads);
	int len1st = getLength(n, size, 0);

	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
	{
		int xst = getStart(len, nthreads, t);
		int xed = getEnd(len, nthreads, t);
		int *curr = count + t * nChunks;
		for (int i = xst; i < xed; i++)
			if (index[i] < len1st) {
				int c = (long) index[i] * nChunks / len1st;
				curr[c] += 1;
			}
	}

	for (int i = 0; i < nChunks; i++)
		for (int j = 1; j < nthreads; j++)
			count[i] += count[i + j * nChunks];

	MPI_Allreduce(MPI_IN_PLACE, count, nChunks, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	rsum[nChunks - 1] = 0;
	for (int i = nChunks - 1; i > 0; i--)
		rsum[i - 1] = rsum[i] + count[i];
	int rem = n;
	for (int i = 0; i < nChunks; i++)
		rem -= count[i];

	int threshold = 0, minCost = n * 2;
	for (int i = 0; i < nChunks; i++) {
		int curr = (long) len1st * (i + 1) / nChunks;
		int cost = curr * 2 + (rem + rsum[i]);
		if (cost < minCost) {
			minCost = cost;
			threshold = curr;
		}
	}
	delete[] count;
	delete[] rsum;

	param.threshold = threshold;

	count = new int[nthreads];
	std::fill(count, count + nthreads, 0);
	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
	{
		int xst = getStart(len, nthreads, t);
		int xed = getEnd(len, nthreads, t);
		for (int i = xst; i < xed; i++)
			if (i + st >= threshold)
				count[t] += mask[i];
	}
	int *offset = new int[nthreads + 1];
	offset[0] = 0;
	for (int i = 0; i < nthreads; i++) {
		offset[i + 1] = offset[i] + count[i];
		count[i] = 0;
	}
	int sendcnt = offset[nthreads]; // number of supervertices
	vector<int> &sind = param.sind;
	sind.resize(sendcnt);
	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
	{
		int xst = getStart(len, nthreads, t);
		int xed = getEnd  (len, nthreads, t);
		for (int i = xst; i < xed; i++)
			if (mask[i] && i + st >= threshold) {
				assert(offset[t] + count[t] < sendcnt);
				sind[offset[t] + count[t]] = i + st;
				count[t] += 1;
			}
	}
	delete[] count;
	delete[] offset;

	int *recvcnt = new int[size];
	int *rdispls = new int[size + 1];
	MPI_Allgather(&sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD);
	rdispls[0] = 0;
	for (int i = 0; i < size; i++)
		rdispls[i + 1] = rdispls[i] + recvcnt[i];
	// allgatherv
	int totrecv = rdispls[size];
	vector<int> &rind = param.rind;
	rind.resize(totrecv);
	MPI_Allgatherv(
			sind.data(), sendcnt, MPIType<I>(),
			rind.data(), recvcnt, rdispls, MPIType<I>(),
			MPI_COMM_WORLD);
	param.totrecv = totrecv;
	delete[] recvcnt;
	delete[] rdispls;
param.t_prepare = MPI_Wtime() - param.t_prepare;
}

template<typename T, typename I = int, typename M = char>
void assign_allreduce (const DistVec<T> &col, const DistVec<I> &index, const DistVec<M> &mask,
		const T &id, FullVec<T> &result, const AssignParam &param)
{
double t1 = MPI_Wtime();
	int rank = param.rank, size = param.size;
	int n = index.getN();
	int st =  getStart (n, size, rank);
	int len = getLength(n, size, rank);
	// allreduce
	int nthreads = get_num_threads();
	int threshold = param.threshold;
	T *mem = new T[threshold * nthreads];
	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
	{
		int xst = getStart(len, nthreads, t);
		int xed = getEnd(len, nthreads, t);
		T *curr = mem + t * threshold;
		std::fill(curr, curr + threshold, id);
		for (int i = xst; i < xed; i++)
			if (mask[i] && index[i] < threshold)
				curr[index[i]] = std::min(curr[index[i]], col[i]);
	}
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < threshold; i++)
	{
		for (int j = 1; j < nthreads; j++)
			mem[i] = std::min(mem[i], mem[j * threshold + i]);
	}
	MPI_Allreduce(mem, result.getPtr(), threshold,
			MPIType<T>(), MPI_MIN, MPI_COMM_WORLD);
	delete[] mem;
double t2 = MPI_Wtime();
	// allgather
	int totrecv = param.totrecv;
	if (totrecv == 0) return;
	int mapsize = param.rind.back() + 1;
	int *rmap = new int[mapsize];
	const vector<int> &sind = param.sind, &rind = param.rind;
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < totrecv; i++)
		rmap[rind[i]] = i;
	T *sval = new T[totrecv];
	std::fill(sval, sval + totrecv, id);
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < len; i++)
		if (mask[i] && index[i] >= threshold) {
			int ind = rmap[index[i]];
			T vNew = col[i];
			T vOld = sval[ind];
			while (vNew < vOld && !__sync_bool_compare_and_swap(sval + ind, vOld, vNew))
				vOld = sval[ind];
		}
	delete[] rmap;
double t3 = MPI_Wtime();

	T *rval = sval;
	MPI_Allreduce(MPI_IN_PLACE, rval, totrecv, MPIType<T>(),
			MPI_MIN, MPI_COMM_WORLD);

double t4 = MPI_Wtime();

	// local assign
	T *res = result.getPtr();
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < totrecv; i++)
	{
		int ind = param.rind[i];
		T vNew = rval[i];
		T vOld = res[ind];
		while (vNew < vOld && !__sync_bool_compare_and_swap(res + ind, vOld, vNew))
			vOld = res[ind];
	}
	delete[] sval;
double t5 = MPI_Wtime();
if (rank == 0)
printf("prepare %f threshold %d assign-1 %f assign-2 %f allgather %f extract %f\n",
	param.t_prepare, threshold, t2-t1, t3-t2, t4-t3, t5-t4);
}

template<typename T, typename I = int, typename M = char>
void assign (const DistVec<T> &col, const DistVec<I> &index, const DistVec<M> &mask,
		const T &id, FullVec<T> &result, const AssignParam &param)
{
	result.fill(id);
	assign_allreduce (col, index, mask, id, result, param);
}

#endif /* API_H_ */
