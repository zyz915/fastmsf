#ifndef DIST_VEC_H_
#define DIST_VEC_H_

#include <mpi.h>

#include "global.h"
#include "range.h"
#include "full_vec.h"

template<typename T>
class DistVec {
public:
	DistVec(int n);
	~DistVec();

	bool operator==(const DistVec<T> &t) const;
	DistVec<T>& operator=(const DistVec<T> &it);
	DistVec<T>& operator=(const FullVec<T> &it);

	T& operator[](size_t i);
	const T& operator[](size_t i) const;

	void fill(const T &v);
	void setToIndex();

	const T *getPtr() const;
	T *getPtr();
	int getN() const;

private:
	int n;
	int rank, size;
	int nthreads;
	int start, end;
	int length;  // number of elements owned by the current process
	int *pRange; // process (whole vector)
	int *eRange; // thread  (each process)
	int *eq_;
	T *ptr_;
};

template<typename T>
DistVec<T>::DistVec(int n):n(n)
{
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	nthreads = get_num_threads();
	pRange = new int[size + 1];
	eRange = new int[nthreads + 1];
	eq_ = new int[nthreads];

	getRange(n, size, pRange);
	start = pRange[rank];
	end = pRange[rank+1];
	length = end - start;
	getRange(length, nthreads, eRange);

	ptr_ = new T[length]; // [start, end)
}

template<typename T>
DistVec<T>::~DistVec()
{
	delete[] pRange;
	delete[] eRange;
	delete[] eq_;
	delete[] ptr_;
}

template<typename T>
bool DistVec<T>::operator==(const DistVec<T> &it) const
{
	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
		eq_[t] = !memcmp(ptr_ + eRange[t], it.ptr_ + eRange[t],
				sizeof(T) * (eRange[t + 1] - eRange[t]));
	int eq = 1;
	for (int t = 0; t < nthreads && eq; t++)
		eq = eq_[t];
	return allReduce(eq, MPI_LAND);
}

template<typename T>
DistVec<T>& DistVec<T>::operator=(const DistVec<T> &it)
{
	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
		std::copy(it.ptr_ + eRange[t], it.ptr_ + eRange[t + 1],
				ptr_ + eRange[t]);
	return *this;
}

template<typename T>
DistVec<T>& DistVec<T>::operator=(const FullVec<T> &it)
{
	const T* it_ptr = it.getPtr() + pRange[rank];
	#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
	for (int t = 0; t < nthreads; t++)
		std::copy(it_ptr + eRange[t], it_ptr + eRange[t + 1],
				ptr_ + eRange[t]);
	return *this;
}

template<typename T>
T& DistVec<T>::operator[](size_t i) {
	return ptr_[i];
}

template<typename T>
const T& DistVec<T>::operator[](size_t i) const {
	return ptr_[i];
}

template<typename T>
void DistVec<T>::fill(const T &v) {
	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < nthreads; t++)
		std::fill(ptr_ + eRange[t], ptr_ + eRange[t + 1], v);
}

template<typename T>
void DistVec<T>::setToIndex() {
	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < nthreads; t++)
		for (int i = eRange[t]; i < eRange[t + 1]; i++)
			ptr_[i] = start + i;
}

template<typename T>
const T* DistVec<T>::getPtr() const {
	return ptr_;
}

template<typename T>
T* DistVec<T>::getPtr() {
	return ptr_;
}

template<typename T>
int DistVec<T>::getN() const {
	return n;
}

#endif /* DIST_VEC_H_ */
