#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <sys/time.h>
#include <cctype>
#include <climits>
#include <mpi.h>
#include <vector>

#include "mpitype.h"

//#ifdef ELONG
using eInt = uint64_t;
//#else
//using eInt = int;
//#endif

// number of threads
static int num_threads = 64;
#ifdef _OPENMP

#include "omp.h"

void set_num_threads(int nt) {
	if (nt > 0) num_threads = nt;
}

int get_num_threads() {
	int thread_max = omp_get_max_threads();
	return (num_threads < thread_max ? num_threads : thread_max);
}

#else

void set_num_threads(int nt) {}
int get_num_threads() { return 1; }

#endif

struct DCSR_Block {
	int      len;
	int64_t  nnz;
	int64_t *pos;     // len+1
	int     *rowidx;  // len
	int     *colidx;  // nnz
	int     *weight;  // nnz

	DCSR_Block():pos(nullptr), rowidx(nullptr),
			colidx(nullptr), weight(nullptr) {}
	~DCSR_Block() {}

	void clear() {
		if (pos) delete[] pos;
		if (rowidx) delete[] rowidx;
		if (colidx) delete[] colidx;
		if (weight) delete[] weight;
		pos = nullptr;
		rowidx = nullptr;
		colidx = nullptr;
		weight = nullptr;
	}
};

struct Graph {
	int n;
	int nblocks;
	DCSR_Block *blkarr;

	int getN() { return n; }
	int64_t getNNZ() {
		int64_t ret = 0;
		for (int i = 0; i < nblocks; i++)
			ret += blkarr[i].nnz;
		return ret;
	}
};

struct CSCGraph {
	int      n;
	eInt     nnz;
	eInt    *pos;     // len
	eInt    *end;     // len
	int     *rowidx;  // nnz
	int     *weight;  // nnz

	int getN() { return n; }
	int64_t getNNZ() { return nnz; }
};

double getTimeSpan(struct timeval t1, struct timeval t2) {
	return
		(t2.tv_sec  - t1.tv_sec ) + 
		(t2.tv_usec - t1.tv_usec) * 1e-6;
}

template<typename T>
T allReduce(T v, MPI_Op op) {
	T ret;
	MPI_Allreduce(&v, &ret, 1, MPIType<T>(), op, MPI_COMM_WORLD);
	return ret;
}

#endif /* GLOBAL_H_ */
