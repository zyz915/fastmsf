#ifndef MPI_TYPE_H_
#define MPI_TYPE_H_

#include <mpi.h>

// prototype
template<typename T> 
MPI_Datatype MPIType();

// char
template<> MPI_Datatype MPIType<char>() {
	return MPI_CHAR;
}

template<> MPI_Datatype MPIType<unsigned char>() {
	return MPI_UNSIGNED_CHAR;
}

// short
template<> MPI_Datatype MPIType<short>() {
	return MPI_SHORT;
}

template<> MPI_Datatype MPIType<unsigned short>() {
	return MPI_UNSIGNED_SHORT;
}

// int
template<> MPI_Datatype MPIType<int>() {
	return MPI_INT;
}

template<> MPI_Datatype MPIType<unsigned int>() {
	return MPI_UNSIGNED;
}

template<> MPI_Datatype MPIType<long>() {
	return MPI_LONG;
}

template<> MPI_Datatype MPIType<unsigned long>() {
	return MPI_UNSIGNED_LONG;
}

// long long
template<> MPI_Datatype MPIType<long long>() {
	return MPI_LONG_LONG;
}

template<> MPI_Datatype MPIType<unsigned long long>() {
	return MPI_UNSIGNED_LONG_LONG;
}

// float
template<> MPI_Datatype MPIType<float>() {
	return MPI_FLOAT;
}

template<> MPI_Datatype MPIType<double>() {
	return MPI_DOUBLE;
}

template<> MPI_Datatype MPIType<long double>() {
	return MPI_LONG_DOUBLE;
}

// boolean
template<> MPI_Datatype MPIType<bool>() {
	return MPI_BYTE;
}

#endif /* MPI_TYPE_H_ */
