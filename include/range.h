#ifndef RANGE_H_
#define RANGE_H_

#include <cassert>
#include <memory>
#include <mpi.h>

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the k-th interval [start, end)
// * the i-th element is in the (i*p/n)-th interval
void getRange(int n, int p, int k, int &start, int &end) {
	uint64_t t = (uint64_t) k * n + p - 1;
	start = t / p;
	end = (t + n) / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the intervals in the array range
// * the i-th interval is [ range[i], range[i+1] )
void getRange(int n, int p, int *range) {
	uint64_t s = p - 1;
	for (int i = 0; i <= p; i++) {
		range[i] = s / p;
		s += n;
	}
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the length of the k-th interval
int getLength(int n, int p, int k) {
	uint64_t t = (uint64_t) k * n + p - 1;
	return (t + n) / p - t / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the start index of the k-th interval
int getStart(int n, int p, int k) {
	return ((uint64_t) n * k + p - 1) / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the end index of the k-th interval
int getEnd(int n, int p, int k) {
	return ((uint64_t) n * (k + 1) + p - 1) / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements. return the interval the given index belongs to
int getRank(int n, int p, int i) {
	return (uint64_t) i * p / n;
}

#endif /* RANGE_H_ */