#ifndef IO_H_
#define IO_H_

#include <mpi.h>
#include <vector>
#include <cassert>
#include <cstdio>
#include <cstdint>
#include <cstring>
#include <sys/stat.h>

#include "range.h"

using namespace std;

struct EdgeUnit {
	int src, dst, weight;
	EdgeUnit() = default;
	EdgeUnit(int s, int d, int w):src(s), dst(d), weight(w) {}
	bool operator<(const EdgeUnit &it) const {
		return (src != it.src ? src < it.src : dst < it.dst);
	}
};

struct Param {
	int type; // 0 = matrix, 1 = binary
	const char *fname;
	int symm, n;
	int nthreads = 1;
};

void tuples2graph(const vector<EdgeUnit> &vec, DCSR_Block &csr, int n)
{
	csr.len = n;
	csr.nnz = vec.size();
	csr.pos = new int64_t[csr.len + 1];
	csr.rowidx = new int[csr.len];
	csr.colidx = new int[csr.nnz];
	csr.weight = new int[csr.nnz];
	int *degree = new int[csr.len];
	fill(degree, degree + csr.len, 0);
	for (const EdgeUnit &e : vec)
		degree[e.src] += 1;
	csr.pos[0] = 0;
	for (int i = 0; i < csr.len; i++)
		csr.pos[i + 1] = csr.pos[i] + degree[i];
	fill(degree, degree + csr.len, 0);
	for (const EdgeUnit &e : vec) {
		int i = e.src;
		csr.rowidx[i] = e.src;
		csr.colidx[csr.pos[i] + degree[i]] = e.dst;
		csr.weight[csr.pos[i] + degree[i]] = e.weight;
		degree[i]++;
	}
	delete[] degree;
}

const int CHUNKSIZE = 1 << 26;
void loadBinary(Param &param, Graph &graph)
{
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int n = param.n;
	int st, ed;
	getRange(n, size, rank, st, ed);
	int len = ed - st;
	int nthreads = get_num_threads();

	FILE *f = fopen(param.fname, "rb");
	if (f == NULL) {
		printf("error! cannot open %s\n", param.fname);
		exit(-1);
	}

	struct stat st_;
	long fsize = 0;
	if (stat(param.fname, &st_) == 0)
		fsize = st_.st_size;

	int dim = nthreads;
	int nblocks = 1 * dim;
	graph.n = n;
	graph.nblocks = nblocks;
	graph.blkarr = new DCSR_Block[nblocks];
	vector<vector<EdgeUnit> > ebuf(nblocks);

	// read the graph	
	int edge_unit_size = sizeof(EdgeUnit);
	assert(edge_unit_size == 12);
	long end = fsize / edge_unit_size, ptr = 0;
	EdgeUnit *es = new EdgeUnit[CHUNKSIZE];

	while (ptr < end) {
		long cnt = (ptr + CHUNKSIZE <= end ? CHUNKSIZE : end - ptr);
		long actual = fread(es, edge_unit_size, cnt, f);
		assert(actual == cnt);

		#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
		for (int t = 0; t < dim; t++)
		{
			int yst = st + getStart(len, dim, t);
			int yed = st + getEnd(len, dim, t);

			for (int i = 0; i < cnt; i++) {
				int x = es[i].src;
				int y = es[i].dst;
				int z = es[i].weight;
				if (y >= yst && y < yed) {
					int v = y - st;
					ebuf[t].push_back(EdgeUnit(x, v, z));
				}
				if (param.symm == 1) {
					swap(x, y);
					if (y >= yst && y < yed) {
						int v = y - st;
						ebuf[t].push_back(EdgeUnit(x, v, z));
					}
				}
			}
		}
		ptr += cnt;
	}
	fclose(f);
	delete[] es;

	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < graph.nblocks; t++) {
		//sort(ebuf[t].begin(), ebuf[t].end());
		tuples2graph(ebuf[t], graph.blkarr[t], n);
		ebuf[t].clear();
	}
}

void loadMatrix(Param &param, Graph &graph)
{
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	char buff[1024];
	FILE *f = fopen(param.fname, "r");
	if (f == NULL) {
		if (rank == 0)
			printf("error! cannot open %s\n", param.fname);
		exit(-1);
	}

	while (fgets(buff, 1024, f))
		if (*buff != '%') break;
	int r, c;
	long nnz;
	sscanf(buff, "%d%d%ld", &r, &c, &nnz);
	if (r != c) {
		if (rank == 0)
			printf("%s is not a square matrix!\n", param.fname);
		exit(0);
	}
	if (rank == 0)
		printf("|V| %d |E| %ld\n", r, nnz);

	int n = r;
	param.n = n;
	int st, ed;
	getRange(n, size, rank, st, ed);
	int len = ed - st;
	int nthreads = get_num_threads();

	int *perm = new int[n];
	for (int i = 0; i < n; i++) perm[i] = i;
	std::random_shuffle(perm, perm + n);

	int dim = nthreads;
	int nblocks = dim;
	graph.n = n;
	graph.nblocks = nblocks;
	graph.blkarr = new DCSR_Block[nblocks];
	vector<vector<EdgeUnit> > ebuf(nblocks);

	while (fgets(buff, 1024, f)) {
		int x = 0, y = 0, z = 0;
		char *p = buff;
		do {
			x = (x << 3) + (x << 1) + ((*p++) - '0');
		} while (isdigit(*p));
		p++;
		do {
			y = (y << 3) + (y << 1) + ((*p++) - '0');
		} while (isdigit(*p));
		p++;
		while (isdigit(*p)) {
			z = (z << 3) + (z << 1) + ((*p++) - '0');
		}
		x = perm[x - 1];
		y = perm[y - 1];
		if (y >= st && y < ed) {
			int v = y - st;
			int c = v * dim / len;
			ebuf[c].push_back(EdgeUnit(x, v, z));
		}
		if (param.symm == 1) {
			swap(x, y);
			if (y >= st && y < ed) {
				int v = y - st;
				int c = v * dim / len;
				ebuf[c].push_back(EdgeUnit(x, v, z));
			}
		}
	}
	fclose(f);
	delete[] perm;

	#pragma omp parallel for num_threads(nthreads) schedule(static)
	for (int t = 0; t < graph.nblocks; t++) {
		tuples2graph(ebuf[t], graph.blkarr[t], n);
		ebuf[t].clear();
	}
}

void loadGraph(Param &param, Graph &csr)
{
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank == 0)
		printf("filename: %s\n", param.fname);

	double t1 = MPI_Wtime();
	switch (param.type) {
		case 0: {
			loadMatrix(param, csr);
			break;
		}
		case 1: {
			loadBinary(param, csr);
			break;
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
	double t2 = MPI_Wtime();
	if (rank == 0)
		printf("finish reading: %fs\n", t2 - t1);
}

void parse(const char *buff, int &x, int &y, int &z, int isReal) {
	x = 0;
	y = 0;
	const char *p = buff;
	do {
		x = (x << 3) + (x << 1) + ((*p++) - '0');
	} while (isdigit(*p));
	p++;
	do {
		y = (y << 3) + (y << 1) + ((*p++) - '0');
	} while (isdigit(*p));
	p++;
	if (isReal) {
		z = int(atof(p) * 65536);
	}
	else {
		z = 0;
		while (isdigit(*p)) {
			z = (z << 3) + (z << 1) + ((*p++) - '0');
		}
	}
}

void loadMatrix(Param &param, CSCGraph &graph) {
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	char buff[1024];
	FILE *f = fopen(param.fname, "r");
	if (f == NULL) {
		if (rank == 0)
			printf("error! cannot open %s\n", param.fname);
		exit(-1);
	}

	char header[100];
	char *h = fgets(header, 100, f);
	assert(header[0] == '%' && header[1] == '%');
	int ptr = 0, spaces = 0;
	while (spaces < 3) {
		spaces += (isspace(header[ptr++]));
	}
	for (int i = ptr; !isspace(header[i]); i++)
		header[i] = tolower(header[i]);
	int isReal = !strncmp("real", header + ptr, 4);

	while (fgets(buff, 1024, f))
		if (*buff != '%') break;
	int r, c;
	long nnz;
	sscanf(buff, "%d%d%ld", &r, &c, &nnz);
	if (r != c) {
		if (rank == 0)
			printf("%s is not a square matrix!\n", param.fname);
		exit(0);
	}
	if (rank == 0)
		printf("|V| %d |E| %ld\n", r, nnz);

	int n = r;
	param.n = n;
	int st, ed;
	getRange(n, size, rank, st, ed);
	int len = ed - st;
	int nthreads = get_num_threads();

	int *perm = new int[n];
	for (int i = 0; i < n; i++) perm[i] = i;
	std::random_shuffle(perm, perm + n);

	int dim = nthreads;
	int nblocks = dim;
	vector<EdgeUnit> ebuf;

	int x, y, z;
	while (fgets(buff, 1024, f)) {
		parse(buff, x, y, z, isReal);
		x = perm[x - 1];
		y = perm[y - 1];
		if (y >= st && y < ed)
			ebuf.push_back(EdgeUnit(x, y-st, z));
		if (param.symm == 1) {
			swap(x, y);
			if (y >= st && y < ed)
				ebuf.push_back(EdgeUnit(x, y-st, z));
		}
	}
	fclose(f);
	delete[] perm;

	nnz = ebuf.size();
	graph.n = n;
	graph.nnz = nnz;
	graph.pos = new eInt[len + 1];
	graph.end = new eInt[len + 1];
	graph.rowidx = new int[nnz];
	graph.weight = new int[nnz];

	int *degree = new int[len];
	eInt *offset = graph.pos;
	std::fill(degree, degree + len, 0);

	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < ebuf.size(); i++) {
		__sync_fetch_and_add(degree + ebuf[i].dst, 1);
	}

	offset[0] = 0;
	for (int i = 0; i < len; i++) {
		graph.end[i] = offset[i + 1] = offset[i] + degree[i];
		degree[i] = 0;
	}

	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < ebuf.size(); i++) {
		int y = ebuf[i].dst;
		eInt l = offset[y] + __sync_fetch_and_add(degree + y, 1);
		graph.rowidx[l] = ebuf[i].src;
		graph.weight[l] = ebuf[i].weight;
	}
	delete[] degree;
}

void loadBinary(Param &param, CSCGraph &graph) {
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int n = param.n;
	int st, ed;
	getRange(n, size, rank, st, ed);
	int len = ed - st;
	int nthreads = get_num_threads();

	FILE *f = fopen(param.fname, "rb");
	if (f == NULL) {
		printf("error! cannot open %s\n", param.fname);
		exit(-1);
	}

	struct stat st_;
	long fsize = 0;
	if (stat(param.fname, &st_) == 0)
		fsize = st_.st_size;

	graph.n = n;
	vector<EdgeUnit> ebuf;

	// read the graph	
	int edge_unit_size = sizeof(EdgeUnit);
	assert(edge_unit_size == 12);
	long end = fsize / edge_unit_size, ptr = 0;
	EdgeUnit *es = new EdgeUnit[CHUNKSIZE];

	while (ptr < end) {
		long cnt = (ptr + CHUNKSIZE <= end ? CHUNKSIZE : end - ptr);
		long actual = fread(es, edge_unit_size, cnt, f);
		assert(actual == cnt);

		for (int i = 0; i < cnt; i++) {
			int x = es[i].src;
			int y = es[i].dst;
			int z = es[i].weight;
			if (y >= st && y < ed)
				ebuf.push_back(EdgeUnit(x, y-st, z));
			if (param.symm == 1) {
				swap(x, y);
				if (y >= st && y < ed)
					ebuf.push_back(EdgeUnit(x, y-st, z));
			}
		}
		ptr += cnt;
	}
	fclose(f);
	delete[] es;

	eInt nnz = ebuf.size();
	graph.n = n;
	graph.nnz = nnz;
	graph.pos = new eInt[len + 1];
	graph.end = new eInt[len + 1];
	graph.rowidx = new int[nnz];
	graph.weight = new int[nnz];

	int *degree = new int[len];
	eInt *offset = graph.pos;
	std::fill(degree, degree + len, 0);

	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < ebuf.size(); i++) {
		__sync_fetch_and_add(degree + ebuf[i].dst, 1);
	}

	offset[0] = 0;
	for (int i = 0; i < len; i++) {
		graph.end[i] = offset[i + 1] = offset[i] + degree[i];
		degree[i] = 0;
	}

	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < ebuf.size(); i++) {
		int y = ebuf[i].dst;
		eInt l = offset[y] + __sync_fetch_and_add(degree + y, 1);
		graph.rowidx[l] = ebuf[i].src;
		graph.weight[l] = ebuf[i].weight;
	}
	delete[] degree;
}

int nextLn(char *buff, int pos, int end) {
	while (pos < end && buff[pos] != '\n') pos++;
	return (pos >= end ? -1 : pos);
}

template<typename MsgT>
long long all_to_all(vector<vector<MsgT> > &sendbuf, vector<vector<MsgT> > &recvbuf, int np, int me) {
	int *sendcnt = new int[np];
	int *recvcnt = new int[np];

	for (int i = 0; i < np; i++)
		sendcnt[i] = sendbuf[i].size();
	//MPI_Alltoall(sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD);

	MPI_Status status;
	int elem = sizeof(MsgT);
	MPI_Datatype type;
	MPI_Type_contiguous(elem, MPI_CHAR, &type);
	MPI_Type_commit(&type);

	for (int i = 1; i < np; i++)
	{
		int fr = (me + np - i) % np;
		int to = (me + i) % np;
		MPI_Sendrecv(
				&sendcnt[to], 1, MPI_INT, to, 0,
				&recvcnt[fr], 1, MPI_INT, fr, 0,
				MPI_COMM_WORLD, &status);
		recvbuf[fr].resize(recvcnt[fr]);
		MPI_Sendrecv(
				sendbuf[to].data(), sendcnt[to], type, to, 0,
				recvbuf[fr].data(), recvcnt[fr], type, fr, 0,
				MPI_COMM_WORLD, &status);
		sendbuf[to].clear();
	}
	recvbuf[me].swap(sendbuf[me]);

	long long totsend = 0;
	for (int i = 0; i < np; i++)
		totsend += sendcnt[i];
	//long long ret = all_sum_ll(totsend);

	long long ret = (totsend - sendcnt[me]) * elem;
	delete[] sendcnt;
	delete[] recvcnt;
	MPI_Type_free(&type);
	return ret;
}

void ParallelReadMM(Param &param, CSCGraph &graph)
{
	char *buff = NULL;
	const char *fname = param.fname;
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int64_t mem64[5];
	std::fill(mem64, mem64 + 5, 0);
	int64_t &nrows = mem64[0];
	int64_t &ncols = mem64[1];
	int64_t &nonzeros = mem64[2];

	if (rank == 0)
	{
		struct stat st;
		stat(fname, &st);
		mem64[4] = st.st_size; // file size
		
		buff = new char[1024];
		FILE *fin = fopen(fname, "r");
		while (fgets(buff, 1024, fin) != NULL) {
			mem64[3] += strlen(buff);
			if (buff[0] != '%') break;
		}
		sscanf(buff, "%lld%lld%lld", mem64, mem64+1, mem64+2);
		delete[] buff;
	}

	mem64[3]--; // characters
	MPI_Bcast(&mem64, 5, MPIType<int64_t>(), 0, MPI_COMM_WORLD);

	int n = nrows;
	param.n = n;
	int *perm = new int[n];
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < n; i++)
		perm[i] = i;
	std::random_shuffle(perm, perm + n);

	int *range = new int[size + 1];
	getRange(n, size, range);

	vector<vector<EdgeUnit> > sendbuf(size), recvbuf(size);
	const int chunksize = (1 << 24);
	const int blocksize = chunksize + 64;
	buff = new char[blocksize];

	MPI_File f_mpi = NULL;
	MPI_Status status;
	MPI_File_open(MPI_COMM_WORLD, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &f_mpi);

	MPI_Offset pos = mem64[3] + chunksize * rank, end = mem64[4];
	while (pos < end) {
		int bytes_read = 0, loc = 0;
		MPI_File_read_at(f_mpi, pos, buff, blocksize, MPI_CHAR, &status);
		MPI_Get_count(&status, MPI_CHAR, &bytes_read);
		buff[bytes_read] = '\n';
		while (loc < bytes_read)
		{
			int ln = nextLn(buff, loc, bytes_read);
			if (ln == -1 || ln >= chunksize) break;
			char *p = buff + (ln + 1);
			if (!isdigit(*p)) break;
			int x = 0, y = 0, z = 0;
			do {
				x = (x << 3) + (x << 1) + ((*p++) - '0');
			} while (isdigit(*p));
			p++;
			do {
				y = (y << 3) + (y << 1) + ((*p++) - '0');
			} while (isdigit(*p));
			if (*p != '\n') {
				p++;
				z = int(atof(p) * 65536);
			}
			// in case!!!
			if (x<1||x>n) continue;
			if (y<1||y>n) continue;
			x = perm[x - 1];
			y = perm[y - 1];
			int r = getRank(n, size, y);
			sendbuf[r].push_back(EdgeUnit(x, y-range[r], z));
			if (param.symm == 1) {
				swap(x, y);
				r = getRank(n, size, y);
				sendbuf[r].push_back(EdgeUnit(x, y-range[r], z));
			}
			while (*p != '\n') p++;
			loc = p - buff;
		}
		pos += chunksize * size;
	}
	MPI_File_close(&f_mpi);
	delete[] perm;
	delete[] buff;
	delete[] range;

	all_to_all(sendbuf, recvbuf, size, rank);
	sendbuf.clear();

	eInt nnz = 0;
	for (int j = 0; j < size; j++)
		nnz += recvbuf[j].size();

	int len = getLength(n, size, rank);
	graph.n = n;
	graph.nnz = nnz;
	graph.pos = new eInt[len + 1];
	graph.end = new eInt[len + 1];
	graph.rowidx = new int[nnz];
	graph.weight = new int[nnz];

	int *degree = new int[len];
	eInt *offset = graph.pos;
	std::fill(degree, degree + len, 0);

	#pragma omp parallel for num_threads(nthreads)
	for (int j = 0; j < size; j++) {
		vector<EdgeUnit> &ebuf = recvbuf[j];
		for (int i = 0; i < ebuf.size(); i++)
			__sync_fetch_and_add(degree + ebuf[i].dst, 1);
	}

	offset[0] = 0;
	for (int i = 0; i < len; i++) {
		graph.end[i] = offset[i + 1] = offset[i] + degree[i];
		degree[i] = 0;
	}

	#pragma omp parallel for num_threads(nthreads)
	for (int j = 0; j < size; j++) {
		vector<EdgeUnit> &ebuf = recvbuf[j];
		for (int i = 0; i < ebuf.size(); i++) {
			int y = ebuf[i].dst;
			eInt l = offset[y] + __sync_fetch_and_add(degree + y, 1);
			graph.rowidx[l] = ebuf[i].src;
			graph.weight[l] = ebuf[i].weight;
		}
	}
	delete[] degree;
}

void loadGraph(Param &param, CSCGraph &csc)
{
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank == 0)
		printf("filename: %s\n", param.fname);

	double t1 = MPI_Wtime();
	switch (param.type) {
		case 0: {
			// loadMatrix(param, csc);
			ParallelReadMM(param, csc);
			break;
		}
		case 1: {
			loadBinary(param, csc);
			break;
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
	double t2 = MPI_Wtime();
	if (rank == 0)
		printf("finish reading: %fs\n", t2 - t1);
}

// symm = 0 or 1: whether the graph should be symmetrized
void usage(char *argv0) {
	printf("usage: %s <file.mtx> <symm> <nthreads>\n", argv0);
	printf("usage: %s <file.wel> <#vertices> <symm> <nthreads>\n", argv0);
	exit(0);	
}

Param parseParam(int argc, char **argv)
{
	if (argc < 2) usage(argv[0]);
	Param param;
	param.fname = argv[1];
	param.symm = 0;
	int l = strlen(param.fname);
	if (l > 4 && !strcmp(param.fname + l - 4, ".mtx")) {
		param.type = 0; // matrix
		if (argc > 2)
			param.symm = atoi(argv[2]);
		if (argc > 3) {
			param.nthreads = atoi(argv[3]);
			set_num_threads(param.nthreads);
		}
	} else {
		param.type = 1; // binary
		if (argc < 3) usage(argv[0]);
		param.n = atoi(argv[2]);
		if (argc > 3)
			param.symm = atoi(argv[3]);
		if (argc > 4) {
			param.nthreads = atoi(argv[4]);
			set_num_threads(param.nthreads);
		}
	}
	return param;
}

#endif /* IO_H_ */
