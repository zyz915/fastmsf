#include <mpi.h>
#include <algorithm>
#include <climits>
#include <numeric>
#include <sys/time.h>

#include "api.h"
#include "range.h"
#include "global.h"
#include "dist_vec.h"
#include "full_vec.h"
#include "io.h"

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	Param param = parseParam(argc, argv);
	Graph graph;
	loadGraph(param, graph);

	int n = param.n;
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int st, ed;
	getRange(n, size, rank, st, ed);
	int len = ed - st;

	// compute
	double t1 = MPI_Wtime();

	FullVec<int> F(n); //, GP(n);
	DistVec<int> m(n), f(n), g(n);
	DistVec<char> mask(n);

	// int nthreads = get_num_threads();
	int nthreads = get_num_threads();
	//GP.setToIndex();
	F.setToIndex();

double t_mxv = 0, t_assign = 0, t_extract = 0, t_others;
double p1, p2;

	int converged = 0, iteration;
	for (iteration = 1; !converged; iteration++)
	{
		int nRoots = 0;
		f = F;
		if (iteration == 1) {
p1 = MPI_Wtime();
			m.setToIndex();
			#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
			for (int b = 0; b < graph.nblocks; b++) {
				auto &block = graph.blkarr[b];
				for (int i = 0; i < block.len; i++) {
					int u = block.rowidx[i];
					for (int64_t j = block.pos[i]; j < block.pos[i + 1]; j++) {
						int v = block.colidx[j];
						m[v] = std::min(u, m[v]);
					}
				}
			}
p2 = MPI_Wtime();
t_mxv = p2 - p1;
			sync_sendrecv(m, F);
		} else {
p1 = MPI_Wtime();
			#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
			for (int b = 0; b < graph.nblocks; b++) {
				auto &block = graph.blkarr[b];
				for (int i = 0; i < block.len; i++) {
					int u = block.rowidx[i];
					for (int64_t j = block.pos[i]; j < block.pos[i + 1]; j++) {
						int v = block.colidx[j];
						m[v] = std::min(F[u], m[v]);
					}
				}
			}
p2 = MPI_Wtime();
t_mxv = p2 - p1;
p1 = MPI_Wtime();
			#pragma omp parallel for num_threads(nthreads) reduction(+:nRoots) schedule(static, 1)
			for (int v = 0; v < len; v++) {
				mask[v] = (f[v] == v + st);
				nRoots += mask[v];
			}
			assign(m, mask, f, F); // F[f[i]] = min(F[f[i]], m[i])
p2 = MPI_Wtime();
t_assign = p2 - p1;
/*
			#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
			for (int v = 0; v < len; v++) {
				m[v] = std::min(m[v], GP[v + st]);
				// m[v] = std::min(m[v],  F[v + st]);
			}
*/
			//sync_sendrecv(m, F);
			sync_sendrecv_sparse(m, F);
		}
p1 = MPI_Wtime();
		// path compression
		#pragma omp parallel for num_threads(nthreads)
		for (int u = 0; u < n; u++)
			if (mask[u])
				while (F[F[u]] != F[u])
					F[u] = F[F[u]];
		#pragma omp parallel for num_threads(nthreads)
		for (int u = 0; u < n; u++)
			F[u] = F[F[u]];
p2 = MPI_Wtime();
t_extract = p2 - p1;
if (rank == 0)
printf("iteration %d: mxv %f assign %f extract %f others %f active %d\n",
iteration, t_mxv, t_assign, t_extract, t_others, nRoots);
p1 = MPI_Wtime();
		g = F;
		converged = (g == f);
p2 = MPI_Wtime();
t_others = p2 - p1;
	}
	double t2 = MPI_Wtime();
	int nCC = 0;
	for (int i = st; i < ed; i++)
		nCC += (F[i] == i);
	nCC = allReduce(nCC, MPI_SUM);
	if (rank == 0) {
		printf("number of components: %d\n", nCC);
		printf("elapsed time: %f\n", t2 - t1);
		printSyncTime();
	}
	MPI_Finalize();
	return 0;
}
