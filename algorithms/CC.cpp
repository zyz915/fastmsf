#include <mpi.h>
#include <algorithm>
#include <climits>
#include <numeric>
#include <sys/time.h>

#include "api.h"
#include "range.h"
#include "global.h"
#include "dist_vec.h"
#include "full_vec.h"
#include "io.h"

int main(int argc, char **argv)
{
	int level;
	MPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE, &level);
	Param param = parseParam(argc, argv);
	// Graph graph;
	CSCGraph graph;
	loadGraph(param, graph);

	int n = param.n;
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int st, ed;
	getRange(n, size, rank, st, ed);
	int len = ed - st;

	// compute
	double t1 = MPI_Wtime();

	FullVec<int> F(n), P(n);
	DistVec<int> f(n), c(n), p(n);
	DistVec<char> mask(n), sup(n);

	// int nthreads = get_num_threads();
	int nthreads = get_num_threads();

	uint64_t inf = (((uint64_t) INT_MAX) << 32) | ((uint64_t) INT_MAX);
	uint64_t sum = allReduce(graph.getNNZ(), MPI_SUM);

	F.setToIndex();
	mask.fill(1); // in a non-isolated tree

double p1, p2;
double t_mxv = 0, t_assign = 0, t_update = 0, t_path = 0;

	int iteration;
	for (iteration = 1; sum != 0; iteration++)
	{
		// vertex's minimum edge selection
		// for each v, e[v] is the minimum value (w, f[u])
		if (iteration == 1) {
p1 = MPI_Wtime();
			c.fill(n);
			#pragma omp parallel for num_threads(nthreads)
			for (int v = 0; v < len; v++)
				for (eInt i = graph.pos[v]; i < graph.end[v]; i++) {
					int u = graph.rowidx[i];
					c[v] = std::min(F[u], c[v]);
				}
p2 = MPI_Wtime();
t_mxv = p2 - p1;
			sync_allgather(c, P);
			f = F;
			#pragma omp parallel for num_threads(nthreads)
			for (int i = 0; i < len; i++)
				sup[i] = (c[i] != i + st);
			//sync_bcast(c, P);
		} else {
p1 = MPI_Wtime();
			// edge removal
			c.fill(n);
			eInt l = 0;
			#pragma omp parallel for num_threads(nthreads) reduction(+:l)
			for (int v = 0; v < len; v++) {
				eInt start = graph.pos[v];
				for (eInt i = graph.pos[v]; i < graph.end[v]; i++) {
					int u = graph.rowidx[i];
					if (F[u] != F[v + st]) {
						graph.rowidx[start++] = u;
						c[v] = std::min(F[u], c[v]);
					}
				}
				graph.end[v] = start;
				if (start > graph.pos[v])
					l += (start - graph.pos[v]);
			}
			graph.nnz = l;
			sum = allReduce(graph.getNNZ(), MPI_SUM);
p2 = MPI_Wtime();
t_mxv = p2 - p1;
			if (sum == 0) {
if (rank == 0)
printf("iteration %d: nvals %ld mxv %f\n", iteration, (long) sum, t_mxv);
				break;
			}
p1 = MPI_Wtime();
			f = F;
			AssignParam ap;
			get_assign_param(f, sup, ap);
			assign(c, f, mask, n, P, ap);
			#pragma omp parallel for num_threads(nthreads)
			for (int i = 0; i < len; i++)
				c[i] = P[f[i]];
p2 = MPI_Wtime();
t_assign = p2 - p1;
			sync_sendrecv(c, P);
			//sync_bcast(c, P);
		}
p1 = MPI_Wtime();
		#pragma omp parallel for num_threads(nthreads)
		for (int u = 0; u < n; u++)
			if (F[u] == u && P[u] != n)
				F[u] = P[u];
		#pragma omp parallel for num_threads(nthreads)
		for (int i = 0; i < len; i++) {
			sup[i] = (f[i] == i + st) && (F[i + st] != i + st); // current non-isolated supervertex
			mask[i] = mask[i] && (F[f[i]] != f[i]); // vertex in a non-isolated spanning tree
		}
		// supervertex selection
		#pragma omp parallel for num_threads(nthreads)
		for (int u = 0; u < n; u++)
			if (u < F[u] && F[F[u]] == u)
				F[u] = u;
p2 = MPI_Wtime();
t_update = p2 - p1;

p1 = MPI_Wtime();
		// path compression
		#pragma omp parallel for num_threads(nthreads)
		for (int u = 0; u < n; u++)
			while (F[F[u]] != F[u])
				F[u] = F[F[u]];
		// non-isolated vertices
		int active = 0;
		#pragma omp parallel for num_threads(nthreads) reduction(+:active)
		for (int i = 0; i < len; i++) {
			sup[i] = sup[i] && (F[i + st] == i + st);
			active += sup[i];
		}
		active = allReduce(active, MPI_SUM);
p2 = MPI_Wtime();
t_path = p2 - p1;

if (rank == 0)	
printf("iteration %d: nvals %ld mxv %f assign %f others %f flattening %f active %d\n",
iteration, (long) sum, t_mxv, t_assign, t_update, t_path, active);
		if (active < 2) break;
	}
	double t2 = MPI_Wtime();
	int nCC = 0;
	for (int i = st; i < ed; i++)
		nCC += (F[i] == i);
	nCC = allReduce(nCC, MPI_SUM);
	if (rank == 0) {
		printf("number of components: %d\n", nCC);
		printf("elapsed time: %f\n", t2 - t1);
		printSyncTime();
	}
	MPI_Finalize();
	return 0;
}
