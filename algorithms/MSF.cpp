#include <mpi.h>
#include <algorithm>
#include <climits>
#include <numeric>
#include <sys/time.h>

#include "api.h"
#include "range.h"
#include "global.h"
#include "dist_vec.h"
#include "full_vec.h"
#include "io.h"

int main(int argc, char **argv)
{
	int level;
	MPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE, &level);
	Param param = parseParam(argc, argv);
	CSCGraph graph;
	loadGraph(param, graph);

	int n = param.n;
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	int st, ed;
	getRange(n, size, rank, st, ed);
	int len = ed - st;

	// compute
	double t1 = MPI_Wtime();

	FullVec<int> F(n), C(n);
	DistVec<int> f(n), c(n), p(n);
	FullVec<uint64_t> E(n);
	DistVec<uint64_t> e(n);
	DistVec<char> mask(n), sup(n);
	//std::vector<EdgeUnit> tuples;

	// int nthreads = get_num_threads();
	int nthreads = get_num_threads();
	uint64_t inf = (((uint64_t) INT_MAX) << 32) | ((uint64_t) INT_MAX);
	uint64_t sum = allReduce(graph.getNNZ(), MPI_SUM);
	int nEdges = 0;
	uint64_t totalWeight = 0;

	F.setToIndex();
	mask.fill(1);
	sup.fill(1);

double p1, p2;
double t_mxv = 0, t_assign = 0, t_update = 0, t_path = 0;

	for (int iteration = 1; sum != 0; iteration++)
	{
		if (iteration == 1)
		{
p1 = MPI_Wtime();
			// vertex's minimum edge selection
			// for each v, e[v] is the minimum value (w, f[u])
			e.fill(inf);
			c.setToIndex();
			#pragma omp parallel for num_threads(nthreads)
			for (int v = 0; v < len; v++)
				for (int64_t i = graph.pos[v]; i < graph.pos[v + 1]; i++) {
					int u = graph.rowidx[i], we = graph.weight[i];
					uint64_t k = ((uint64_t) we << 32) ^ F[u];
					if (k < e[v]) {
						e[v] = k;
						c[v] = u;
					}
				}
			/*
			#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
			for (int b = 0; b < graph.nblocks; b++) {
				auto &block = graph.blkarr[b];
				for (int i = 0; i < block.len; i++) {
					int u = block.rowidx[i];
					for (int64_t j = block.pos[i]; j < block.pos[i + 1]; j++) {
						int v = block.colidx[j], we = block.weight[j];
						uint64_t k = ((uint64_t) we << 32) ^ F[u];
						if (k < e[v]) {
							e[v] = k;
							c[v] = u;
						}
					}
				}
			}
			*/
p2 = MPI_Wtime();
t_mxv = p2 - p1;
			sync_allgather(c, F);
p1 = MPI_Wtime();
			#pragma omp parallel for num_threads(nthreads)
			for (int v = 0; v < len; v++) {
				sup[v] = sup[v] && (c[v] != v + st);
				mask[v] = (c[v] != v + st);
			}
			#pragma omp parallel for num_threads(nthreads)
			for (int u = 0; u < n; u++)
				if (u < F[u] && F[F[u]] == u)
					F[u] = u;
p2 = MPI_Wtime();
t_update = p2 - p1;
			// extract
			int weight = 0;
			#pragma omp parallel for num_threads(nthreads) reduction(+:weight)
			for (int v = 0; v < len; v++)
				if (F[v + st] != v + st) {
					int we = int(e[v] >> 32);
					// tuples.push_back(EdgeUnit(c[v], v, we));
					weight += we;
				}
			totalWeight += weight;
		} else {
p1 = MPI_Wtime();
			// edge removal
			e.fill(inf);
			c.setToIndex();
			int64_t l = 0;
			#pragma omp parallel for num_threads(nthreads) reduction(+:l)
			for (int v = 0; v < len; v++) {
				int64_t start = graph.pos[v];
				for (int64_t i = graph.pos[v]; i < graph.end[v]; i++) {
					int u = graph.rowidx[i], we = graph.weight[i];
					if (F[u] != F[v + st]) {
						graph.rowidx[start] = u;
						graph.weight[start] = we;
						start++;
						uint64_t k = ((uint64_t) we << 32) ^ F[u];
						if (k < e[v]) {
							e[v] = k;
							c[v] = u;
						}
					}
				}
				graph.end[v] = start;
				if (start > graph.pos[v])
					l += (start - graph.pos[v]);
			}
			graph.nnz = l;
			/*
			#pragma omp parallel for num_threads(nthreads) schedule(static, 1)
			for (int b = 0; b < graph.nblocks; b++) {
				auto &block = graph.blkarr[b];
				int64_t l = 0;
				for (int i = 0; i < block.len; i++) {
					int u = block.rowidx[i];
					int64_t start = block.pos[i];
					block.pos[i] = l;
					for (int64_t j = start; j < block.pos[i + 1]; j++) {
						int v = block.colidx[j], we = block.weight[j];
						if (F[u] != F[v + st]) {
							block.colidx[l] = v;
							block.weight[l] = we;
							l++;
							uint64_t k = ((uint64_t) we << 32) ^ F[u];
							if (k < e[v]) {
								e[v] = k;
								c[v] = u;
							}
						}
					}
				}
				block.pos[block.len] = l;
				block.nnz = l;
			}
			*/
			sum = allReduce(graph.getNNZ(), MPI_SUM);
p2 = MPI_Wtime();
t_mxv = p2 - p1;
			if (sum == 0) {
if (rank == 0)	
printf("iteration %d: nvals %ld mxv %f\n", iteration, (long) sum, t_mxv);
				break;
			}
p1 = MPI_Wtime();
			f = F;
			AssignParam ap;
			get_assign_param(f, sup, ap);
			assign(e, f, mask, inf, E, ap);
			// extract
			p.fill(n);
			#pragma omp parallel for num_threads(nthreads)
			for (int v = 0; v < len; v++) {
				uint64_t k = E[f[v]];
				if (k != inf && k == e[v])
					p[v] = v + st;
			}
			assign(p, f, mask, n, C, ap);
p2 = MPI_Wtime();
t_assign = p2 - p1;
p1 = MPI_Wtime();
			// update f
			#pragma omp parallel for num_threads(nthreads)
			for (int u = 0; u < n; u++)
				if (F[u] == u && E[u] != inf)
					F[u] = int(E[u]);
			// non-isolated vertices
			#pragma omp parallel for num_threads(nthreads)
			for (int i = 0; i < len; i++) {
				sup[i] = sup[i] && (F[i + st] != i + st);
				mask[i] = mask[i] && (F[f[i]] != f[i]);
			}
			// supervertex selection
			#pragma omp parallel for num_threads(nthreads)
			for (int u = 0; u < n; u++)
				if (u < F[u] && F[F[u]] == u) {
					C[u] = n;			
					F[u] = u;
				}
p2 = MPI_Wtime();
t_update = p2 - p1;
			// extract
			int nTuples = 0, weight = 0;
			#pragma omp parallel for num_threads(nthreads) reduction(+:nTuples,weight)
			for (int v = 0; v < len; v++)
				if (C[f[v]] == v + st) {
					int we = int(E[f[v]] >> 32);
					// tuples.push_back(EdgeUnit(p[v], v, we));
					nTuples += 1;
					weight += we;
				}
			nEdges += nTuples;
			totalWeight += weight;
		}
p1 = MPI_Wtime();
		// path compression
		#pragma omp parallel for num_threads(nthreads)
		for (int u = 0; u < n; u++)
		{
			int gp = F[F[u]];
			while (F[u] != gp) {
				F[u] = gp;
				gp = F[gp];
			}
		}
		int active = 0;
		#pragma omp parallel for num_threads(nthreads) reduction(+:active)
		for (int i = 0; i < len; i++) {
			sup[i] = sup[i] && (F[i + st] == i + st);
			active += sup[i];
		}
		active = allReduce(active, MPI_SUM);
p2 = MPI_Wtime();
t_path = p2 - p1;
if (rank == 0)	
printf("iteration %d: nvals %ld mxv %f assign %f others %f flattening %f active %d\n",
iteration, (long) sum, t_mxv, t_assign, t_update, t_path, active);
		if (active < 2) break;
	}
	double t2 = MPI_Wtime();
	int nCC = 0;
	for (int i = st; i < ed; i++)
		nCC += (F[i] == i);
	nCC = allReduce(nCC, MPI_SUM);
	nEdges = allReduce(nEdges, MPI_SUM);
	totalWeight = allReduce(totalWeight, MPI_SUM);
//printf("nEdges %d nCC %d n %d\n", nEdges, nCC, n);
//	assert(nEdges + nCC == n);
	if (rank == 0) {
		printf("number of components: %d\n", nCC);
		printf("elapsed time: %f\n", t2 - t1);
		printf("total weight: %ld\n", (long) totalWeight);
		printSyncTime();
	}
	MPI_Finalize();
	return 0;
}
